/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testesocketcliente;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;  
import java.io.InputStream;
import java.io.PrintStream;  
import java.net.Socket;  
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
  
public class Cliente {  
  
    public static void main(String[] args) {
        JanelaCliente c = new JanelaCliente();
        c.setVisible(true);
        //c.conexao();
        /*int filesize=6022386;  
        try{
            long start = System.currentTimeMillis();  
            int bytesRead;  
            int current = 0;
            
            System.out.println("Iniciando protocolo FTP...");
            System.out.println("Tentando abrir conexão com server...");
            
            //Aceita a conexão e abre o canal de comunicação
            Socket sock = new Socket("127.0.0.1",21);  

            System.out.println("Conexão aceita...");
            
            // recebendo o arquivo  
            
            System.out.println("Direcionando arquivo...");
            
            //tamanho do arquivo
            byte [] mybytearray  = new byte [filesize];  
            InputStream in = sock.getInputStream();
            
            //cria arquivo
            FileOutputStream fos = new FileOutputStream("teste.rar");  
            BufferedOutputStream bos = new BufferedOutputStream(fos);  
            bytesRead = in.read(mybytearray,0,mybytearray.length);  
            current = bytesRead;
            double tamanho = in.available();//Tamanho do arquivo
            
            System.out.println("Transferindo o arquivo...");
            
            //Começa transferencia via protocolo tcp
            do {  
               bytesRead = in.read(mybytearray, current, (mybytearray.length-current));
               if(bytesRead >= 0) current += bytesRead;  
            } while(bytesRead > -1);  

            System.out.println("Arquivo transferido!");
            
            //grava arquivo no disco
            bos.write(mybytearray, 0 , current);  
            long end = System.currentTimeMillis();  
            System.out.println("Tempo de transferencia: "+(end-start));  
            
            //fecha o arquivo
            bos.close();  
            
            System.out.println("Fechando conexão com server");
            
            //fecha conexao com server
            sock.close();
            
            System.out.println("Conexão encerrada! arquivo transferido com sucesso!");
            //unzip();
        }catch(Exception e){
            System.out.println("Sem conexão...");
        }*/
    }
    
    public static void unzip(){
        
        Unzip u = new Unzip();
        File f = new File("teste.zip");
        File d = new File(f.getAbsolutePath().replace("teste.zip", ""));
        try {
            u.extrairZip(f, d);
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        f.delete();
        
    }
    
}  