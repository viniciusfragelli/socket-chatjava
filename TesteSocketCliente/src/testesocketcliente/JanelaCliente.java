/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testesocketcliente;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.Socket;

/**
 *
 * @author vinicius
 */
public class JanelaCliente extends javax.swing.JFrame {

    /**
     * Creates new form JanelaCliente
     */
    public JanelaCliente() {
        initComponents();
        setLocationRelativeTo(null);
        
    }

    public void conexao(){
        int filesize=6022386;
        String t;
        try{
            long start = System.currentTimeMillis();  
            int bytesRead;  
            int current = 0;
            
            t = "Iniciando protocolo FTP...\n";
            text.setText(t);
            
            t += "Tentando abrir conexão com server...\n";
            text.setText(t);
            
            //Aceita a conexão e abre o canal de comunicação
            Socket sock = new Socket("127.0.0.1",21);  

            t += "Conexão aceita...\n";
            text.setText(t);
            
            // recebendo o arquivo  
            
            t += "Direcionando arquivo...\n";
            text.setText(t);
            
            //tamanho do arquivo
            byte [] mybytearray  = new byte [filesize];  
            InputStream in = sock.getInputStream();
            
            //cria arquivo
            FileOutputStream fos = new FileOutputStream("teste.rar");  
            BufferedOutputStream bos = new BufferedOutputStream(fos);  
            bytesRead = in.read(mybytearray,0,mybytearray.length);  
            current = bytesRead;
            double tamanho = in.available();//Tamanho do arquivo
            
            t += "Transferindo o arquivo...\n";
            text.setText(t);
            
            //Começa transferencia via protocolo tcp
            do {  
               bytesRead = in.read(mybytearray, current, (mybytearray.length-current));
               if(bytesRead >= 0) current += bytesRead;  
            } while(bytesRead > -1);  

            t += "Arquivo transferido!\n";
            text.setText(t);
            
            //grava arquivo no disco
            bos.write(mybytearray, 0 , current);  
            long end = System.currentTimeMillis();  
            t += "Tempo de transferencia: "+(end-start)+"\n";
            text.setText(t);
            
            //fecha o arquivo
            bos.close();  
            
            t += "Fechando conexão com server\n";
            text.setText(t);
            
            //fecha conexao com server
            sock.close();
            
            t += "Conexão encerrada!\narquivo transferido com sucesso!\n";
            text.setText(t);
            //unzip();
        }catch(Exception e){
            System.out.println("Sem conexão...");
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        text = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        text.setColumns(20);
        text.setRows(5);
        jScrollPane1.setViewportView(text);

        jButton1.setText("Receber");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(151, 151, 151)
                .addComponent(jButton1)
                .addContainerGap(176, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        conexao();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JanelaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JanelaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JanelaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JanelaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JanelaCliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea text;
    // End of variables declaration//GEN-END:variables
}
