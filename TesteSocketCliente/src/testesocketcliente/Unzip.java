/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testesocketcliente;

/**
 *
 * @author Vinicius
 */

import java.io.*;

import java.util.*;

import java.util.zip.*;

import java.text.*;

public class Unzip {

    private final int TAMANHO_BUFFER = 2048;
    
    public Unzip() {
    }
    
    public void extrairZip( File arquivoZip, File diretorio ) throws ZipException, IOException {  
        ZipFile zip = null;  
        File arquivo = null;  
        InputStream is = null;  
        OutputStream os = null;  
        String pasta = System.getProperty("user.home"); 
        byte[] buffer = new byte[TAMANHO_BUFFER];  
        try {  
          //cria diretório informado, caso não exista  
          if( !diretorio.exists() ) {  
            diretorio.mkdirs();  
          }  
          if( !diretorio.exists() || !diretorio.isDirectory() ) {  
            throw new IOException("Informe um diretório válido");  
          }  
          zip = new ZipFile( arquivoZip );  
          Enumeration e = zip.entries();  
          while( e.hasMoreElements() ) {  
            ZipEntry entrada = (ZipEntry) e.nextElement();  
            arquivo = new File( diretorio, entrada.getName() );  
            //se for diretório inexistente, cria a estrutura   
            //e pula pra próxima entrada  
            if( entrada.isDirectory() && !arquivo.exists() ) {  
              arquivo.mkdirs();  
              continue;  
            }  
            //se a estrutura de diretórios não existe, cria  
            if( !arquivo.getParentFile().exists() ) {  
              arquivo.getParentFile().mkdirs();  
            }  
            try {  
              //lê o arquivo do zip e grava em disco  
              is = zip.getInputStream( entrada );
              if(!arquivo.isDirectory()){
                os = new FileOutputStream( arquivo );  
                int bytesLidos = 0;  
                if( is == null ) {  
                  throw new ZipException("Erro ao ler a entrada do zip: "+entrada.getName());  
                }  
                while( (bytesLidos = is.read( buffer )) > 0 ) {  
                  os.write( buffer, 0, bytesLidos );  
                }  
              }
            } finally {  
              if( is != null ) {  
                try {  
                  is.close();  
                } catch( Exception ex ) {}  
              }  
              if( os != null ) {  
                try {  
                  os.close();  
                } catch( Exception ex ) {}  
              }  
            }  
          }  
        } finally {  
          if( zip != null ) {  
            try {  
              zip.close();  
            } catch( Exception e ) {}  
          }  
        }  
  }
    
    
    public static void unzip(ZipInputStream zin, String s) throws IOException {

    System.out.println("unzipping " + s);

    FileOutputStream out = new FileOutputStream(s);

    byte [] b = new byte[512];

    int len = 0;

    while ( (len=zin.read(b))!= -1 ) {

      out.write(b,0,len);

      }

    out.close();

    }
    
}
